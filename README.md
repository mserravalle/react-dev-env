# React development environment.

## Create your app structure

Only the first time you need run

```
docker-compose run react-app npx create-react-app app
```

Enjoy the development!. Server: http://localhost:3001

## Run with docker-compose up

To run the command you must change the root in docker-compose into volumes

change:

```
volumes:
  - ./:/app
```

to

```
volumes:
  - ./myAppName:/app
```

and then run the project with docker-compose up.