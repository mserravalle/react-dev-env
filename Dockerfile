# pull official base image
FROM node:lts-alpine

RUN mkdir /app
WORKDIR /app
ADD . /app